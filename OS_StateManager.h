/*
 * OS_StateManager.h
 *
 *  Created on: 25.04.2021
 *      Author: kraemere
 */

#ifndef _OS_STATEMANAGER_H_
#define _OS_STATEMANAGER_H_


/********************************* includes **********************************/
#include "BaseTypes.h"
#include "OS_EventManager.h"
#include "OS_State_Reset.h"
    
#include "StateList.h"

/***************************** defines / macros ******************************/

//Use of X-Macros for defining OS-relevant states
/*              State name                          |       State-Entry-Fn              |           State-Root-Fn          |         State-Exit-Fn          */
#define OS_STATE_LIST \
   STATE(   eSM_State_NoState                       ,    NULL                           ,       NULL                       ,      NULL                   )\
   STATE(   eSM_State_Reset                         ,    OS_State_Reset_Entry           ,       OS_State_Reset_Root        ,      OS_State_Reset_Exit    )


/****************************** type definitions *****************************/

// Generate an enum list for the error list
typedef enum
{
    #ifdef OS_STATE_LIST
        #define STATE(StateName, EntryFn, RootFn, ExitFn) StateName,
            OS_STATE_LIST
        #undef STATE
    #endif
    #ifdef USER_STATE_LIST
        #define STATE(StateName, EntryFn, RootFn, ExitFn) StateName,
            USER_STATE_LIST
        #undef STATE
    #endif
    eStateListLastEntry
}teStateList;


/* Typedef for StateFunction pointers */
typedef u8 (*pucStateFnPointer)(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2);

typedef enum
{
    eStateStatus_EntryEnter,        /* Set this state when entry-state is entered */
    eStateStatus_EntryReached,      /* Set this state when entry-state is reached */
    eStateStatus_RootEnter,         /* Set this state when root-state is entered */
    eStateStatus_RootReached,       /* Set this state when entry-state is reached */
    eStateStatus_ExitEnter,         /* Set this state when exit-state is entered */
    eStateStatus_ExitReached        /* Set this state when entry-state is reached */
}teSM_StateStatus;


/* Structure is filled by a X-Macro */
typedef struct
{
    pucStateFnPointer pucFnEntry;       /* Function pointer for entry function of this state */
    pucStateFnPointer pucFnRoot;        /* Function pointer for main or root function of this state */
    pucStateFnPointer pucFnExit;        /* Function pointer for exit function of this state */
}tsSM_States;


typedef struct
{
    pucStateFnPointer pucFnCurrentState;    /* Function pointer to the current state */
    pucStateFnPointer pucFnNextState;       /* Function pointer to the next state */
    pucStateFnPointer pucFnRequestState;    /* Function pointer to the requested state */
    teSM_StateStatus  eStateStatus;         /* Represents the current status of the state */
    teStateList       eCurrentState;        /* Enumeration of the current active state */
}tsSM_Handler;




/***************************** global variables ******************************/
// Use the uiError array for the listing
extern const tsSM_States sSM_States[];


/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif

void        OS_StateManager_Init(void);
u8          OS_StateManager_Handle(teEventID eEvtParm, uiEventParam1 uiEvtParam1, ulEventParam2 ulEvtParam2);
teStateList OS_StateManager_GetCurrentState(void);
void        OS_StateManager_ChangeState(teStateList eRequestState);
void        OS_StateManager_RequestNextState(void);
void        OS_StateManager_CurrentStateReached(void);

#ifdef __cplusplus
}
#endif

#endif /* _OS_STATEMANAGER_H_ */
