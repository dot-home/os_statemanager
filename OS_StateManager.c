//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019

 \file       OS_StateManager.c
 \brief      Provides functions for a state machine handler

 ***********************************************************************************/
#include "OS_Config.h"


#include "OS_StateManager.h"
#include "OS_ErrorDebouncer.h"

/****************************************** Defines ******************************************************/


/****************************************** Variables ****************************************************/
/* Structure array to hold the information about the states in the system */
const tsSM_States sSM_States[eStateListLastEntry] =
{
    #ifdef OS_STATE_LIST
        #define STATE(StateName, StateEntryFn, StateRootFn, StateExitFn) {StateEntryFn, StateRootFn, StateExitFn},
            OS_STATE_LIST
        #undef STATE
    #endif
    #ifdef USER_STATE_LIST
        #define STATE(StateName, StateEntryFn, StateRootFn, StateExitFn) {StateEntryFn, StateRootFn, StateExitFn},
            USER_STATE_LIST
        #undef STATE
    #endif
};

/* Main handler of the state machine */
static tsSM_Handler sSM_Handler;


/****************************************** Function prototypes ******************************************/


/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author  KraemerE
\date    19.02.2019
\brief   Checks the next available state according to the requested state.
\param   none
\return  none
***********************************************************************************/
static void CheckForNextState(void)
{
    /* Check if state is already reached */
    if(sSM_Handler.pucFnCurrentState != sSM_Handler.pucFnRequestState)
    {
        /* Check if the requested state is in the same array-spot */
        u8 ucStateIdx;
        u8 ucIndexOfCurrentState = eStateListLastEntry;
        u8 ucIndexOfRequestState = eStateListLastEntry;

        for(ucStateIdx = 0; ucStateIdx < _countof(sSM_States); ucStateIdx++)
        {
            /* Search for requested state index*/
            if(sSM_States[ucStateIdx].pucFnRoot == sSM_Handler.pucFnRequestState)
            {
                //Current state index is interesting
                ucIndexOfRequestState = ucStateIdx;
            }

            /* Search for current state index */
            if( (sSM_States[ucStateIdx].pucFnEntry == sSM_Handler.pucFnCurrentState)
                || (sSM_States[ucStateIdx].pucFnRoot == sSM_Handler.pucFnCurrentState)
                || (sSM_States[ucStateIdx].pucFnExit == sSM_Handler.pucFnCurrentState))
            {
                ucIndexOfCurrentState = ucStateIdx;
            }
        }

        /* When indexes are same, than the requested state is in the same array entry. Therefore one of the three states */
        /* Current state and requested state differs in the array entry */
        /* Check for the next state */
        if(sSM_Handler.pucFnCurrentState == sSM_States[ucIndexOfCurrentState].pucFnEntry)
        {
            /* Check if entry status has already been reached */
            if(sSM_Handler.eStateStatus == eStateStatus_EntryReached)
            {
                /* Next logical state from entry is root-state */
                if(sSM_Handler.pucFnNextState == sSM_States[ucIndexOfCurrentState].pucFnRoot)
                {
                    /* Set current state to root state and next state to exit state */
                    sSM_Handler.pucFnCurrentState = sSM_States[ucIndexOfCurrentState].pucFnRoot;
                    sSM_Handler.pucFnNextState = sSM_States[ucIndexOfCurrentState].pucFnExit;
                    sSM_Handler.eStateStatus = eStateStatus_RootEnter;
                    sSM_Handler.eCurrentState = (teStateList)ucIndexOfCurrentState;
                }
                else
                {
                    OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_RootNxtState);
                }
            }
        }
        else if(sSM_Handler.pucFnCurrentState == sSM_States[ucIndexOfCurrentState].pucFnRoot)
        {
            /* Check if root status has already been reached */
            if(sSM_Handler.eStateStatus == eStateStatus_RootReached)
            {
                /* Next logical state from root is exit-state */
                if(sSM_Handler.pucFnNextState == sSM_States[ucIndexOfCurrentState].pucFnExit)
                {
                    /* Set current state to exit state and the next state to entry state */
                    sSM_Handler.pucFnCurrentState = sSM_States[ucIndexOfCurrentState].pucFnExit;
                    sSM_Handler.pucFnNextState = sSM_States[ucIndexOfRequestState].pucFnEntry;
                    sSM_Handler.eStateStatus = eStateStatus_ExitEnter;
                    sSM_Handler.eCurrentState = (teStateList)ucIndexOfCurrentState;
                }
                else
                {
                    OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_ExitNxtState);
                }
            }
        }

        else if(sSM_Handler.pucFnCurrentState == sSM_States[ucIndexOfCurrentState].pucFnExit)
        {
            /* Check if root status has already been reached */
            if(sSM_Handler.eStateStatus == eStateStatus_ExitReached)
            {
                /* Next logical state from exit state is the entry-state of another array-entry-state */
                if(sSM_Handler.pucFnNextState == sSM_States[ucIndexOfRequestState].pucFnEntry)
                {
                    /* Set current state to exit state and the next state to entry state */
                    sSM_Handler.pucFnCurrentState = sSM_States[ucIndexOfRequestState].pucFnEntry;
                    sSM_Handler.pucFnNextState = sSM_States[ucIndexOfRequestState].pucFnRoot;
                    sSM_Handler.eStateStatus = eStateStatus_EntryEnter;
                    sSM_Handler.eCurrentState = (teStateList)ucIndexOfRequestState;
                }
                else
                {
                    OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_EntryNxtState);
                }
            }
        }
        else
        {
            OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_NoState);
        }
    }
}

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       28.04.2021
\brief      Initializes the SM_Handler structure to be used in Reset-State.
\return     none
\param      none
***********************************************************************************/
void OS_StateManager_Init(void)
{
    sSM_Handler.pucFnCurrentState = sSM_States[eSM_State_Reset].pucFnEntry;
    sSM_Handler.pucFnNextState = sSM_States[eSM_State_Reset].pucFnRoot;
    sSM_Handler.pucFnRequestState = sSM_States[eSM_State_Reset].pucFnRoot;
    sSM_Handler.eStateStatus = eStateStatus_EntryEnter;
    sSM_Handler.eCurrentState = eSM_State_Reset;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       28.04.2021
\brief      Calls the set state-function and checks afterwards if a state change has to be done.
\return     ucReturnVal - Return value of the state function.
\param      eEvtParm - Event parameter which shall be handled in the new state.
\param      uiEvtParam1 - Event parameter data
\param      ulEvtParam2 - Event parameter data
***********************************************************************************/
u8 OS_StateManager_Handle(teEventID eEvtParm, uiEventParam1 uiEvtParam1, ulEventParam2 ulEvtParam2)
{
    u8 ucReturnVal = 0;

    /* Call the actual standby function */
    if(sSM_Handler.pucFnCurrentState)
    {
        ucReturnVal = sSM_Handler.pucFnCurrentState(eEvtParm, uiEvtParam1, ulEvtParam2);
    }

    /* Get the next state */
    CheckForNextState();

    return ucReturnVal;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       28.04.2021
\brief      Returns the current state of the state machine as an enumeration.
\return     eCurrentState - The current state where the state machine points to.
\param      none
***********************************************************************************/
teStateList OS_StateManager_GetCurrentState(void)
{
    return sSM_Handler.eCurrentState;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       28.04.2021
\brief      Changes the requested root state. Checks first for valid request state.
\return     none
\param      eRequestState - The new state which shall be used for root-state
***********************************************************************************/
void OS_StateManager_ChangeState(teStateList eRequestState)
{
    /* Check for valid state change request */
    if(eSM_State_NoState < eRequestState && eRequestState < eStateListLastEntry)
    {
        sSM_Handler.pucFnRequestState = sSM_States[eRequestState].pucFnRoot;
    }
    else
    {
        OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_InvalidRequest);
    }
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       28.04.2021
\brief      Changes the request state to the next available state.
            This function shall only be used in the OS-States
\return     none
\param      none
***********************************************************************************/
void OS_StateManager_RequestNextState(void)
{
    /* State change is only allowe on OS-States. */
    if(sSM_Handler.eCurrentState <= eSM_State_Reset)
    {
        /* Get the next requested state */
        teStateList eNextState = sSM_Handler.eCurrentState + 1;
        
        /* Check if the next state is valid */
        if(eNextState < eStateListLastEntry)
        {        
            OS_EVT_PostEvent(eEvtState_Request, eNextState, 0);
        }
    }
    else
    {
        OS_ErrorDebouncer_PutErrorInQueue(eStateMachineError_InvalidRequest);
    }
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.05.2021
\brief      Current state reached the desired state. Switch to next requested state.
\return     none
\param      none
***********************************************************************************/
void OS_StateManager_CurrentStateReached(void)
{
    if(sSM_Handler.pucFnCurrentState != sSM_Handler.pucFnRequestState)
    {
        switch(sSM_Handler.eStateStatus)
        {
            case eStateStatus_EntryEnter:
            {
                sSM_Handler.eStateStatus = eStateStatus_EntryReached;
                break;
            }
            case eStateStatus_RootEnter:
            {
                sSM_Handler.eStateStatus = eStateStatus_RootReached;
                break;
            }
            case eStateStatus_ExitEnter:
            {
                sSM_Handler.eStateStatus = eStateStatus_ExitReached;
                break;
            }

            default:
                break;
        }
    }
}

